import React from 'react';
import logo from '../../assets/logo.png';
import './header.scss';

function HeaderView() {
  return (
    <header className="header">
        <img src={logo} alt="SkyHub logo" className="logo"/>
    </header>
  );
}

export default HeaderView;
