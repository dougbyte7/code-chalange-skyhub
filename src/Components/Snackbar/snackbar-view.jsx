import React, { useContext, useEffect } from 'react';
import './snackbar.scss';
import { ReportContext } from '../../Routes/App';

const clearMessage = (snackbar, dispatch) => {
    dispatch({type: 'UPDATE_MESSAGE', data: ''})
    snackbar.classList.toggle("snackbar--fade");
}

function SnackbarView() {
    const {state, dispatch} = useContext(ReportContext);

    const snackbar = React.createRef();

    useEffect(() => {
        if(!state.message) return;

        const snack = snackbar.current;
        if(snack.classList.contains("snackbar--fade")) return;

        snack.classList.toggle("snackbar--fade");
        setTimeout(() => clearMessage(snack, dispatch), 3650);
        
    }, [state.message]);

    return (
        <div className="snackbar" ref={snackbar}>{state.message}</div>
    );
}

export default SnackbarView;
