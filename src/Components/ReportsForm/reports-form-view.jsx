import React from 'react';
import './reports-form.scss'
import { ReactComponent as FormIcon} from '../../assets/article.svg';
import { ReactComponent as TrashIcon} from '../../assets/trash.svg';
import Select from '../FormComponents/Select';
import Date from '../FormComponents/Date';

function ReportsFormView(props) {
    return(
        <section className="report-form-container">
            <div className="report-form-title-container">
                <FormIcon className="report-form-icon"/>
                <h1 className="report-form-title">Solicite novos relatórios</h1>
            </div>
            <form className="report-form" onSubmit={event => props.getReports(event)}>
                <Select id="platform" label="Plataformas" options={props.options} required="required"/>
                <Date id="startDate" label="Data Inicial"/>
                <Date id="endDate" label="Data Final"/>
                <button className="report-form__btn-clear" type="button" onClick={(e) => props.clearFilters(e.target.parentElement)}>
                    <TrashIcon className="report-form__trash-icon"/>
                    Limpar filtros
                </button>
                <button className="report-form__btn-get-report" type="submit">Solicitar relatório</button>
            </form>
        </section>
    );
};

export default ReportsFormView;