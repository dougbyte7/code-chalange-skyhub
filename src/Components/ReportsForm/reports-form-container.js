import React, {useState, useEffect, useContext} from 'react';
import ReportsFormView from './reports-form-view';
import ApiService from '../../Services/Api/ApiService';
import { ReportContext } from '../../Routes/App';

function ReportsForm() {

    const {state, dispatch} = useContext(ReportContext);
    const [options, setOptions] = useState([]);
    const [platforms, setPlatforms] = useState([]);
    
    useEffect(()=> {
        ApiService.getOptions()
        .then(data => {
            setPlatforms(data);
            return data;
        }
        ).then(data => 
            data.map(platform => platform.name)
        ).then(options =>
            setOptions(options)
        ).catch(error => console.log(error.message));
    }, []);

    const clearFilters = (form) => {
        form.reset();
    };

    const validateForm = filters => {
        if(!filters.platform_id || !filters.start_date || !filters.end_date) {
            dispatch({type: 'UPDATE_MESSAGE', data: 'Todos os campos são obrigatórios'});
            return false;
        }
        if(new Date(filters.end_date) < new Date(filters.start_date)) {
            dispatch({type: 'UPDATE_MESSAGE', data: 'A data inicial não pode ser maior que a data final'});
            return false;
        }
        
        return true;
    }

    const getReports = (event) => {
        event.preventDefault();
        event.stopPropagation();

        const form = event.target;

        const filters = { 
            platform_id: platforms.filter(platform => form.querySelector('#platform').value === platform.name)
                .map(platform => platform.id)[0],
            start_date: new Date(form.querySelector('#startDate').value).toJSON(),
            end_date: new Date(form.querySelector('#endDate').value).toJSON()
        };
        
        if(!validateForm(filters)) return;

        const btn = form.querySelector('.report-form__btn-get-report');
        btn.disabled = true;

        const postReports = ApiService.postReports(filters);

        const getReports = ApiService.getReports()
            .then(reports => reports.filter(report => {
                return report.filters.platform_id === filters.platform_id &&
                    (new Date(report.filters.start_date) >= new Date(filters.start_date) || 
                     new Date(report.filters.end_date)   <= new Date(filters.end_date));
            })
            ).then(reports => dispatch({type: 'UPDATE_REPORTS', data: reports}))
            .then(() => btn.disabled = false);

        Promise.all([postReports, getReports])
        .catch(error => {
            console.log(error.message);
            dispatch({type: 'UPDATE_MESSAGE', data: 'Algo deu errado, tente outra vez!'})
        });

    }

    return (
        <ReportsFormView options={options} clearFilters={clearFilters} getReports={getReports}/>
    );
}

export default ReportsForm;