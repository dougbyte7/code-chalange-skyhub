import React, { useContext } from 'react';
import { ReportContext } from '../../Routes/App';
import ReportsHistoryView from './reports-history-view';
import ApiService from '../../Services/Api/ApiService';

const downloadReport = (id, name) => {
    ApiService.getReport(id)
    .then(response => response.blob())
    .then( report =>  {
        const url = window.URL.createObjectURL(report);
        
        let a = document.createElement('a');
        a.href = url;
        a.download = `${name}.txt`;
        a.click();
        window.URL.revokeObjectURL(url);
    })
    .catch(error => console.log(error.message));
}

const deleteReport = (id, dispatch, reports, button) => {
    button.disabled = true;
    
    ApiService.deleteReport(id)
    .then(report => {
        dispatch({type: 'UPDATE_REPORTS', data: reports.filter(el => el.id !== id)});
    })
    .then(() => dispatch({type: 'UPDATE_MESSAGE', data: 'Relatório deletado com sucesso!'}))
    .catch(error => {
        console.log(error.message);
        dispatch({type: 'UPDATE_MESSAGE', data: 'Algo deu errado, tente outra vez!'});
        button.disabled = false
    });
}

function ReportsHistory() {
    const { state, dispatch } = useContext(ReportContext);

    return(
        <ReportsHistoryView reports={state.reports} downloadReport={downloadReport} deleteReport={deleteReport} dispatch={dispatch}/>
    );
}

export default ReportsHistory;
