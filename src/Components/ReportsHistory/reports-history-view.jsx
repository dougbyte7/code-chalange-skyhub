import React from 'react';
import './reports-history.scss';
import { ReactComponent as HistoryIcon } from '../../assets/to-do.svg';
import { ReactComponent as DownloadIcon } from '../../assets/download.svg';
import { ReactComponent as TrashIcon } from '../../assets/trash.svg';
import { ReactComponent as CheckmarkIcon } from '../../assets/checkmark.svg';
import { ReactComponent as CrossIcon } from '../../assets/cross.svg';

function getDate(dateString) {
    const date = new Date(dateString);
    return `${date.getUTCDate()}/${date.getUTCMonth()+1}/${date.getUTCFullYear()}`;
}

function getStatus(status) {
    return status === 'Created' ? 
    <div className="report-status"><CheckmarkIcon className="report-history-icon report-history-icon--color-success"/><p className="report-history-table__status">Concluído</p></div>
    : <div className="report-status"><CrossIcon className="report-history-icon report-history-icon--color-red"/><p className="report-history-table__status">Erro</p></div>;
}

function getReports(reports, downloadReport, deleteReport, dispatch) {
    if(reports) { 
        return reports.map((report, i, array) =>  
            <tr key={report.id} className="report-history-table__row">
                <td className="report-history-table__item report-history-table__item__date ">{getDate(report.createAt)}</td>
                <td className="report-history-table__item">{report.name}</td>
                <td className="report-history-table__item">{getStatus(report.status)}</td>
                <td className="report-history-table__item report-history-table__item--center">
                    <button className="report-history__btn" onClick={() => downloadReport(report.id, report.name)} type="button"> <DownloadIcon className="report-history-icon report-history-icon--color-download"/></button>
                </td>
                <td className="report-history-table__item report-history-table__item--center">
                    <button className="report-history__btn" onClick={(event) => deleteReport(report.id, dispatch, array, event.currentTarget)} type="button"><TrashIcon className="report-history-icon report-history-icon--color-red"/></button>
                </td>
            </tr> 
        );
    }
}

function ReportsHistoryView(props) {

    return (
        <section className="report-history-container">
            <div className="report-history-title-container">
                <HistoryIcon className="report-history-title-icon"/>
                <h1 className="report-history-title">Histórico de relatórios</h1>
            </div>
        
            <table className="report-history-table">
                <thead>
                    <tr className="report-history-table__row">
                        <th className="report-history-table__item">Data</th>
                        <th className="report-history-table__item">Detalhes</th>
                        <th className="report-history-table__item">Status</th>
                        <th className="report-history-table__item report-history-table__item--center">Download</th>
                        <th className="report-history-table__item report-history-table__item--center">Excluir</th>
                    </tr>
                </thead>
                <tbody>
                    {getReports(props.reports, props.downloadReport, props.deleteReport, props.dispatch)}
                </tbody>
            </table>
        </section>
    );
}

export default ReportsHistoryView;