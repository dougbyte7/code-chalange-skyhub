import React from 'react';
import './date.scss'

function DateView(props) {
    return(
        <label className="date-label">
            {props.label}
            <input id={props.id || ""} className="date-input" type="date"/>
        </label>
    );
}

export default DateView;