import React from 'react';
import './select.scss';

function SelectView(props) {
    return (
        <label className="select-label">
            {props.label}
            <select id={props.id || ""} className="select-input" required={props.required}>
                <option value="Selecione" defaultValue>Selecione...</option>
                {props.options.map((option, i) => <option key={i} value={option}>{option}</option>)}
            </select>
        </label>
    );
}

export default SelectView;