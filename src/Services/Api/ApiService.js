const headers = new Headers({
    'authorization': '6f51f2d0-6546-4824-b512-35ca69759d74',
    'accept': 'application/json',
    'content-type': 'application/json'
});

const ApiService = {
    getOptions: () => {
        return fetch('http://challenge.skyhub.com.br/platforms', {
            headers: headers,
            method: 'GET'
        })
        .then(response => {
            if(response.ok) {
                return response.json();
            } else {
                throw new Error("Falha ao obter plataformas");
            }
        })
    },

    getReports: () => {
        return fetch('http://challenge.skyhub.com.br/reports', {
            headers: headers,
            method: 'GET'
        })
        .then(response => {
            if(response.ok) {
                return response.json();
            } else {
                throw new Error("Falha ao obter relatórios");
            }
        })
    },

    postReports: filters => {
        return fetch('http://challenge.skyhub.com.br/reports', {
            headers: headers,
            method: "POST",
            body: JSON.stringify(filters)
        })
        .then(response => {
            if(response.ok) {
                return response.json();
            } else {
                throw new Error("Falha ao enviar relatórios");
            }
        })
    },

    getReport: id => {
        return fetch(`http://challenge.skyhub.com.br/reports/${id}`, {headers: {accept: 'text/csv', authorization: '6f51f2d0-6546-4824-b512-35ca69759d74'}, method: 'GET'})
            .then(response => {
                if(response.ok) {
                    return response;
                } else {
                    throw new Error("Falha ao obter relatório");
                }
            })
    },

    deleteReport: id => {
        return fetch(`http://challenge.skyhub.com.br/reports/${id}`, {headers, method: 'DELETE'})
            .then(response => {
                if(response.ok) {
                    return response;
                } else {
                    throw new Error("Falha ao deletar relatório");
                }
            })
    }
}

export default ApiService;