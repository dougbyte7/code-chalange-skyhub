import React, { useReducer } from 'react';
import Header from '../../Components/Header';
import ReportForm from '../../Components/ReportsForm';
import ReportsHistory from '../../Components/ReportsHistory';
import Snackbar from '../../Components/Snackbar';

export const ReportContext = React.createContext();
const initialState = {
    reports: [],
    message: '',
};

function reducer(state, action) {
  switch(action.type) {
    case 'UPDATE_REPORTS':
      return {
        reports: action.data
      };
    case 'UPDATE_MESSAGE':
      return {
        reports: state.reports,
        message: action.data
      }
    default:
      return initialState;
  }
}

function App() {

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div className="App">
      <Header/>
      <main>
        <ReportContext.Provider value={{state, dispatch}}>
          <ReportForm/>
          <ReportsHistory/>
          <Snackbar/>
        </ReportContext.Provider>
      </main>
    </div>
  );
}

export default App;
